all:
	@gmake $@
.PHONY: all build run

# .DEFAULT:
# 	@gmake $@

build:
	go mod tidy
	mkdir -p build/bin
	go build   cmd/api-server/main.go 
	mv	main build/bin

run:
	# user service running on  port : 3333
	 build/bin/main