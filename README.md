# user-api

## Name
Users-api service

## Description
A simple users api CRUD Written in Golang

## Installation
```shell
make build
make run
```


## Usage
#### list of curl to test the endpoints
###### Create User endpoint

###### request
```shell
curl -X POST \
  http://localhost:3333/users \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"email" : "test1@mail.com",
	"address" :{
		"delivery": "delivery address 1",
		"billing": "billing address 2"
	},
	"contact" : {
		"home": "+699235643456",
		"work": "+690123456789",
		"mobile": "+630987654321"
	}
}'
```

###### response
```shell
{
    "Email": "test1@mail.com",
    "Address": {
        "Delivery": "delivery address 1",
        "Billing": "billing address 2"
    },
    "Contact": {
        "Work": "+690123456789",
        "Home": "+699235643456",
        "Mobile": "+630987654321"
    }
}
```

######  GET  user

###### request
```shell
curl -X GET \
  http://localhost:3333/users/test1@mail.com \
  -H 'cache-control: no-cache'
```
  
  ###### response

```shell
{
    "Email": "test1@mail.com",
    "Address": {
        "Delivery": "delivery address 1",
        "Billing": "billing address 2"
    },
    "Contact": {
        "Work": "+690123456789",
        "Home": "+699235643456",
        "Mobile": "+630987654321"
    }
}
```

###### GET ALL user
###### request
```shell
curl -X GET \
  http://localhost:3333/users/ \
  -H 'cache-control: no-cache' 
```

 ###### response
    {
        "marlon09@gmail.com": {
            "Email": "marlon09@gmail.com",
            "Address": {
                "Delivery": "delivery address 1",
                "Billing": "billing address 2"
            },
            "Contact": {
                "Work": "012345342",
                "Home": "231231213",
                "Mobile": "233452323"
            }
        },
        "test02@mail.com": {
            "Email": "test02@mail.com",
            "Address": {
                "Delivery": "delivery address 1",
                "Billing": "billing address 2"
            },
            "Contact": {
                "Work": "+690123456789",
                "Home": "+699235643456",
                "Mobile": "+630987654321"
            }
        },
        "test1@mail.com": {
            "Email": "test1@mail.com",
            "Address": {
                "Delivery": "delivery address 1",
                "Billing": "billing address 2"
            },
            "Contact": {
                "Work": "+690123456789",
                "Home": "+699235643456",
                "Mobile": "+630987654321"
            }
        }
    }


###### Update User
###### request
    curl -X PUT \
      http://localhost:3333/users/test1@mail.com \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \
      -H 'postman-token: 6b176aa9-19f7-c52f-9513-b6c81215d3d8' \
      -d '{
    	"email" : "test1@mail.com",
    	"address" :{
    		"delivery": "delivery address 1",
    		"billing": "billing address 2"
    	},
    	"contact" : {
    		"home": "+699235643456",
    		"work": "+690123456789",
    		"mobile": "+630987654321"
    	}
    }'

###### response
```shell
{
    "Email": "test1@mail.com",
    "Address": {
        "Delivery": "delivery address 1",
        "Billing": "billing address 2"
    },
    "Contact": {
        "Work": "+690123456789",
        "Home": "+699235643456",
        "Mobile": "+630987654321"
    }
}
```

###### DELETE User /multiple
###### request
```shell
curl -X DELETE \
  'http://localhost:3333/users?email_id=marlon01%40gmail.com%2Cmarlon11%40gmail.com' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' 
```




## License
For open source projects, say how it is licensed.