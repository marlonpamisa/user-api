package models

type User struct {
	Email   string
	Address *Address
	Contact *Contact
}
