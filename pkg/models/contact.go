package models

type Contact struct {
	Work,
	Home,
	Mobile string
}
