package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"gitlab.com/marlonpamisa/user-api/pkg/models"
)

type UsersResource struct {
	Users map[string]models.User
}

// Routes creates a REST router for the todos resource
func (rs UsersResource) Routes() chi.Router {
	r := chi.NewRouter()
	rs.Users = make(map[string]models.User, 0)
	r.Get("/", rs.List)    // GET /users - read a list of users
	r.Post("/", rs.Create) // POST /users - create a new user and persist it
	r.Delete("/", rs.Delete)

	r.Route("/{email}", func(r chi.Router) {
		r.Get("/", rs.Get)    // GET /users/{email} - read a single user by :id
		r.Put("/", rs.Update) // PUT /users/{email} - update a single user by :id
	})

	return r

}

func (rs *UsersResource) List(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(rs.Users)
}

func (rs *UsersResource) Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var p models.User
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if _, ok := rs.Users[p.Email]; ok {
		err = errors.New("email already exist")
		http.Error(w, err.Error(), http.StatusConflict)
		return

	}
	rs.Users[p.Email] = p
	json.NewEncoder(w).Encode(p)
}

func (rs *UsersResource) Get(w http.ResponseWriter, r *http.Request) {

	if email := chi.URLParam(r, "email"); email != "" {
		if _, ok := rs.Users[email]; ok {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(rs.Users[email])
			return

		} else {
			err := errors.New("email not exist")
			http.Error(w, err.Error(), http.StatusNotFound)
		}

	}
	http.Error(w, "", http.StatusBadGateway)

}

func (rs UsersResource) Update(w http.ResponseWriter, r *http.Request) {
	if email := chi.URLParam(r, "email"); email != "" {

		if _, ok := rs.Users[email]; ok {

			var p models.User
			err := json.NewDecoder(r.Body).Decode(&p)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			delete(rs.Users, email)
			rs.Users[p.Email] = p
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(rs.Users[p.Email])
			return

		} else {
			err := errors.New("email not exist")
			http.Error(w, err.Error(), http.StatusNotFound)
		}

	}
	http.Error(w, "", http.StatusBadGateway)

}

func (rs *UsersResource) Delete(w http.ResponseWriter, r *http.Request) {
	emailIDs := r.URL.Query().Get("email_id")
	emailIDsArr := strings.Split(emailIDs, ",")
	fmt.Println("emailIDsArr")
	fmt.Println(emailIDsArr)
	for _, emailID := range emailIDsArr {
		delete(rs.Users, emailID)
	}
	w.Header().Set("Content-Type", "application/json")

	if len(emailIDsArr) == 0 {
		http.Error(w, "email id provided", http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	return
}
